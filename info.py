from flask import Flask

app = Flask(__name__)

@app.route('/info', methods=['GET'])
def info():
    return 'Luis Fernando Culajay Sandoval - 201903838'

@app.route('/curso', methods=['GET'])
def curso():
    return 'Analisis y Diseño 1 - USAC'

if __name__ == '__main__':
      app.run(port=4000)