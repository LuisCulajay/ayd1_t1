from flask import Flask, request

app = Flask(__name__)

@app.route('/suma', methods=['POST'])
def suma():
    datos_dict = request.get_json()
    return {
        'res': datos_dict['n1'] + datos_dict['n2'],
    }

@app.route('/resta', methods=['POST'])
def resta():
    datos_dict = request.get_json()
    return {
        'res': datos_dict['n1'] + datos_dict['n2'],
    }

if __name__ == '__main__':
      app.run(port=3000)